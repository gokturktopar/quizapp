import 'package:flutter/material.dart';
import './quiz.dart';
import 'result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;
  final _questions = [
    {
      'qText': 'Q1',
      'options': [
        {'optionText': 'q1o1', 'score': 1},
        {'optionText': 'q1o2', 'score': 2},
      ]
    },
    {
      'qText': 'Q2',
      'options': [
        {'optionText': 'q2o1', 'score': 3},
        {'optionText': 'q2o2', 'score': 4},
      ]
    },
  ];
  void _answerQuestion(int score) {
    setState(() {
      _totalScore += score;
      _questionIndex++;
    });
  }

  void _goBack() {
    setState(() {
      _questionIndex--;
    });
  }

  void _startOver() {
    setState(() {
      _totalScore = 0;
      _questionIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Row(children: <Widget>[
              Text('Quiz', textAlign: TextAlign.right,),
              Text('Total Score: $_totalScore'),
            ]),
          ),
          body: _questionIndex < _questions.length && _questionIndex > -1
              ? Quiz(
                  questions: _questions,
                  answerQuestion: _answerQuestion,
                  goBack: _goBack,
                  questionIndex: _questionIndex,
                )
              : Result(_startOver)),
    );
  }
}
