import 'package:flutter/material.dart';
import 'question.dart';
import 'answer.dart';

class Quiz extends StatelessWidget {
  final List<Map<Object, Object>> questions;
  final int questionIndex;
  final Function answerQuestion;
  final Function goBack;
  Quiz({@required this.questionIndex, @required this.questions, this.answerQuestion, this.goBack});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text('Questions'),
        Question(questions[questionIndex]['qText']),
        ...(questions[questionIndex]['options'] as List<Map<String, Object >>).map(
          (option) => Answer(option['optionText'], () => answerQuestion(option['score'])),
        ),
        RaisedButton(
            child: Text('Back'), color: Colors.red, onPressed: goBack),
      ],
    );
  }
}
