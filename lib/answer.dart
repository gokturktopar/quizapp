import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final String text;
  final Function callback;
  Answer(this.text, this.callback);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Colors.blueGrey,
        textColor: Colors.deepOrange,
        child: Text(text),
        onPressed: callback,
      ),
    );
  }
}
