import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final Function startOver;
  Result(this.startOver);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Center(
          child: Text(
            'Quiz Completed!',
            textAlign: TextAlign.center,
          ),
        ),
        RaisedButton(child: Text('Start Over'), onPressed: startOver)
      ],
    );
  }
}
